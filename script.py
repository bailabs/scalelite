
import boto3
import os
import configparser

config = configparser.ConfigParser()
config.read("{0}/config.ini".format(os.path.dirname(os.path.abspath(__file__))))

stream = os.popen('docker exec -i scalelite-api ./bin/rake status')
output = stream.read()
print(output)
#stream.close()
data = output.split()

max_number_room = 3
max_up = 5

previous_no_of_room = 0

regions = ['ap-southeast-1', 'ap-northeast-2', 'ap-northeast-1']

def turn_off_instance(id, region):
    instances = [id]
    ec2 = boto3.client('ec2', region_name=region)
    response = ec2.stop_instances(InstanceIds=instances)
    print(response)
    print('Stopped instances: ' + str(instances))

def turn_on_instance(id, region):
    instances = [id]
    ec2 = boto3.client('ec2', region_name=region)
    response = ec2.start_instances(InstanceIds=instances)
    print(response)
    print('Started instances: ' + str(instances))

def get_instances_state():
    #dict={}
    #for region in regions:
    #    ec2 = boto3.resource('ec2', region_name=region)
    #    for instance in ec2.instances.all():
    #        #for instance in ec2.meta.client.describe_instance_status()['InstanceStatuses']:
    #        dict[instance.id] = instance.state
    #        print(instance)
    #print(arr['i-0ae94ac86450d775f']['Name'])
    dict={}
    for region in regions:
        ec2 = boto3.resource('ec2', region_name=region)
        for instance in ec2.instances.all():
            #for instance in ec2.meta.client.describe_instance_status()['InstanceStatuses']:
            dict[instance.id] = instance.state
    #print(arr['i-0ae94ac86450d775f']['Name'])
    return dict

if __name__ == "__main__":
    instance_states = get_instances_state()
    for idx, site in enumerate(data):
        site_p = site.split('.')[0]
        if config.has_section(site_p):
            print("================================================")
            print("Checking Server: "+site_p)
            print("Current Room: "+data[idx+3])
            if int(data[idx+3]) >= max_number_room:
                print("Limit reached")
                try:
                    cur_site = str(data[idx+8].split('.')[0])
                    print("Turning up server: "+ cur_site)
                    if str(data[idx+10]) != "online":
                        print("Enabling Server - "+cur_site)
                        print("Staring Server - "+cur_site)
                        if str(instance_states[config[cur_site]['instance_id']]['Name']) != "running":
                            #print("Enabling: "+ cur_site)
                            #stream_req = os.popen('docker exec -i scalelite-api ./bin/rake servers:enable[{0}]'.format(config[cur_site]['server_id']))
                            #output_req = stream_req.read()
                            #print("Enabled: "+output_req)
                            #stream_req.close()
                            turn_on_instance(config[cur_site]['instance_id'], config[cur_site]['region'])
                            config[data[cur_site]]['up'] = "0"
                        else:
                            print("Server is stil starting")
                    else:
                        print(cur_site+ " Server already up")
                except:
                    pass
            else:
                if str(site_p) != "bbb1":
                    current_up = int(config[site_p]['up'])
                    if int(data[idx+3]) > 0:
                       print("Resetting up value")
                       config[site_p]['up'] = "0"
                    else:
                        if previous_no_of_room < max_number_room:
                            if current_up >= max_up:
                                print("Turning off Server")
                                if instance_states[config[site_p]['instance_id']]['Name'] != "stopped":
                                    #stream_req = os.popen('docker exec -i scalelite-api ./bin/rake servers:disable[{0}]'.format(config[site_p]['server_id']))
                                    #output_req = stream_req.read()
                                    #print("Disabled: "+output_req)
                                    #stream_req.close()
                                    turn_off_instance(config[site_p]['instance_id'], config[site_p]['region'])
                                    config[site_p]['up'] = "0"
                                else:
                                    print("Server is down")
                                    config[site_p]['up'] = "0"
                            else:
                                if str(data[idx+2]) == "online":
                                    print("Updating up for server " + site_p)
                                    updated_up = int(config[site_p]['up']) + 1
                                    config[site_p]['up'] = str(updated_up)
                                else:
                                    print("Server is offline")
                                    config[site_p]['up'] = "0"
                        else:
                            print("Not updating up/shutting down server, previous server no. of rooms reached limit")
                print("No action required")
            previous_no_of_room = int(data[idx+3])
            print("================================================")
            print("\n")
    with open("{0}/config.ini".format(os.path.dirname(os.path.abspath(__file__))), 'w') as configfile:
        config.write(configfile)

